# System Multi Agent Jade #

JADE (Java Agent DEvelopment framework) is a middleware for the development of applications, both in the mobile and fixed environment, based on the Peer-to-Peer intelligent autonomous agent approach. JADE enables developers to implement and deploy multi-agent systems, including agents running on wireless networks and limited-resource devices.

System Multi Agent is an example multi-agent system of stockbrokers using JADE framework according to FIPA standard specifications.

### SYSTEM REQUIREMENTS ###
To build the framework a complete Java programming environment is needed. At least a Java Development Kit version 1.4 is required. 

### INSTALLATION ###
You can download JADE in source form and recompile it yourself, or get the pre-compiled binaries (actually they are JAR files). 

References:

- http://jade.tilab.com 

- http://jade.tilab.com/dl.php?file=JADE-all-4.4.0.zip